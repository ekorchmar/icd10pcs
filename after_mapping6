create table attr_from_usagi 
	(
		attr_name varchar(255),
		concept_id number,
		concept_name varchar(255),
		NEW_CODE varchar(255),
		NEW_NAME varchar(255),
		extra varchar(255)
	);
-- upload usagi/manually mapped table
WbImport -file=/home/ekorchmar/i10patos_fu
         -type=text
         -table=ATTR_FROM_USAGI
         -encoding="UTF-8"
         -header=true
         -decode=false
         -dateFormat="yyyy-MM-dd"
         -timestampFormat="yyyy-MM-dd HH:mm:ss"
         -delimiter='\t'
         -quotechar='"'
         -decimal=.
         -fileColumns=ATTR_NAME,CONCEPT_ID,CONCEPT_NAME,NEW_CODE,NEW_NAME,EXTRA
         -quoteCharEscaping=none
         -ignoreIdentityColumns=false
         -deleteTarget=true
         -continueOnError=false
         -batchSize=1000;
;
drop table attr_map;
create table attr_map as
select
	attr_name,
	nvl (c.concept_id, a.concept_id) as concept_id
from attr_from_usagi a
left join concept c on
	a.new_code = c.concept_code and
	c.vocabulary_id = 'SNOMED'
where extra is null
;
delete from attr_map where concept_id is null
;
drop table icd10mappings;
--final *source* table we will work with
create table icd10mappings as
select 
	c.concept_id as procedure_id,
	u.concept_code as procedure_code,
	c.concept_name as procedure_name,
	a.concept_id as attribute_id,
	c2.concept_name as attribute_name,
	c2.concept_class_id
from atr_unil u
join concept c on
	c.vocabulary_id = 'ICD10PCS' and
	c.concept_code = u.concept_code
join attr_map a on
	u.attr_name = a.attr_name
join concept c2 on
	c2.concept_id = a.concept_id
;
exec DBMS_STATS.GATHER_TABLE_STATS (ownname => USER, tabname => 'ICD10MAPPINGS', cascade => true);
CREATE INDEX IDX_ICD10MAPPINGS
   ON ICD10MAPPINGS (ATTRIBUTE_ID ASC, PROCEDURE_ID ASC)
   TABLESPACE USERS;

CREATE INDEX IDX_ICD10MAPPINGS2
   ON ICD10MAPPINGS (PROCEDURE_ID ASC, ATTRIBUTE_ID ASC)
   TABLESPACE USERS;

CREATE INDEX IDX_ICD10MAPPINGS3
   ON ICD10MAPPINGS (CONCEPT_CLASS_ID ASC)
   TABLESPACE USERS
;
drop table false_pairs
;
--remove parent attributes where their children are present, for clarity
create table false_pairs as 
	select 
		i.procedure_id, i.attribute_id
	from icd10mappings i
	join concept_ancestor a	on
		i.attribute_id = a.ancestor_concept_id and a.min_levels_of_separation != 0
	join icd10mappings i2 on
		i2.procedure_id = i.procedure_id and
		a.descendant_concept_id = i2.attribute_id;
		
delete from icd10mappings where (procedure_id, attribute_id) in (select * from false_pairs);
;
--remove procedures with multiple procedure-attributes 
--these can exist due to errors in manual attribute mappings (where something that is not a procedure made one)
--or because of SNOMED hierarchy shortcomings (both logical parent and child were present but not removed in previous step)
create table to_delete as
with x as
	(
		select distinct procedure_id from icd10mappings where concept_class_id = 'Procedure' group by procedure_id having count(distinct attribute_id)>1
	)
select distinct i.PROCEDURE_ID from icd10mappings i
join x on x.procedure_id = i.procedure_id 
where concept_class_id = 'Procedure';

delete from icd10mappings
where procedure_id in
	(select * from to_delete);
	
drop table to_delete;
--create table for all acceptable relations between a procedure and an attribute inside SNOMED
drop table relations;
create table relations (relationship_id varchar (127));
BEGIN
	insert into relations values ('During');
	insert into relations values ('Has direct site');
	insert into relations values ('Has technique');
	insert into relations values ('Followed by');
	insert into relations values ('Using subst');
	insert into relations values ('Using device');
	insert into relations values ('Using energy');
	insert into relations values ('Using acc device');
	insert into relations values ('Occurs after');
	insert into relations values ('Has surgical appr');
	insert into relations values ('Has scale type');
	insert into relations values ('Has property');
	insert into relations values ('Has proc site');
	insert into relations values ('Has indir proc site');
	insert into relations values ('Has dir proc site');
	insert into relations values ('Has proc morph');
	insert into relations values ('Has proc device');
	insert into relations values ('Has proc context');
	insert into relations values ('Has pathology');
	insert into relations values ('Has asso morph');
	insert into relations values ('Has method');
	insert into relations values ('Has specimen');
	insert into relations values ('Has dir subst');
	insert into relations values ('Has dir morph');
	insert into relations values ('Has dir device');
	insert into relations values ('Has asso finding');
	insert into relations values ('Has asso morph');
	insert into relations values ('Has surgical appr');
	insert into relations values ('Has access');
end;
/
;
drop table test_group 
-- this table will contain ALL possible ICD10 to SNOMED matches
-- exactly one match with procedure-attribute or it's descendant
-- at least one match on any other attribute or it's ancestor
;
create table test_group as

select distinct -- parents <3
	i.procedure_id, --icd10 proc
	a.descendant_concept_id as snomed_id, --snomed proc match candidate
	ra.ancestor_concept_id as match_on, --matching attribute id
	ra.min_levels_of_separation as ac, --how far up is the mapping of the attribute, lower number = more precise
	a.max_levels_of_separation as depth, --how far down is the mapping of the procedure, less specific is better (avoid adding detalisation)
	x.concept_class_id -- to store if attribute is a body site; important to take in the account for topographic procedures
from icd10mappings i
join devv5.concept_ancestor a on  --Include all procedure descendants
	i.concept_class_id = 'Procedure' and
	a.ancestor_concept_id = i.attribute_id
	--and substr (i.procedure_code,1,3) = '0R9' --uncomment to test on single code group, useful to check mappings
join icd10mappings x on --place horizontally procedures and all other attributes
	x.procedure_id = i.procedure_id and
	x.concept_class_id != 'Procedure'
join devv5.concept_ancestor ra on --include ancestors of all other attributes
	ra.descendant_concept_id = x.attribute_id and
	ra.max_levels_of_separation <=3 --limit set because of acceptability and RAM limits reasons
join concept_relationship r on -- find matching pairs of procedures and attributes in SNOMED
	r.concept_id_1 = a.descendant_concept_id and
	r.concept_id_2 = ra.ancestor_concept_id and
	r.invalid_reason is null
join relations e on --ensure validness of relationship; excludes other vocabularies and speeds up search
	e.relationship_id = r.relationship_id
join concept s on 
    s.concept_id=r.concept_id_1 and s.vocabulary_id = 'SNOMED'
join concept s1 on 
    s1.concept_id=r.concept_id_2 and s1.vocabulary_id = 'SNOMED'
join umls.mrconso u on --filter non-international SNOMED, lower amount of possible concepts but much higher quality of relation between procedures and attributes
	s.concept_code = CODE and u.sab = 'SNOMEDCT_US'
;

--separate insert -- 5th level exceeds memory limit
insert into test_group
select distinct -- parents <3
	i.procedure_id, --icd10 proc
	a.descendant_concept_id as snomed_id, --snomed proc
	ra.ancestor_concept_id as match_on, --matching attribute id
	ra.min_levels_of_separation as ac, --how far up is the mapping of the attribute, more precise is better
	a.max_levels_of_separation as depth, --how far down is the mapping of the procedure, less specific is better
	x.concept_class_id -- to store if it is a body site
from icd10mappings i
join devv5.concept_ancestor a on
	i.concept_class_id = 'Procedure' and
	a.ancestor_concept_id = i.attribute_id
	--and substr (i.procedure_code,1,3) = '0R9' --uncomment to test on single code group, useful to check mappings
join icd10mappings x on --place horizontally procedures and other attributes
	x.procedure_id = i.procedure_id and
	x.concept_class_id != 'Procedure'
join devv5.concept_ancestor ra on --extend other attributes vertically up
	ra.descendant_concept_id = x.attribute_id and
	ra.max_levels_of_separation in (4,5) --limit set because of acceptability and RAM limits reasons
join concept_relationship r on --find procedure(-child) which has attribute(-parent)
	r.concept_id_1 = a.descendant_concept_id and
	r.concept_id_2 = ra.ancestor_concept_id and
	r.invalid_reason is null
join relations e on --ensure validness of relationship
	e.relationship_id = r.relationship_id
join concept s on 
    s.concept_id=r.concept_id_1 and s.vocabulary_id = 'SNOMED'
join concept s1 on 
    s1.concept_id=r.concept_id_2 and s1.vocabulary_id = 'SNOMED'
join umls.mrconso u on --filter non-international SNOMED, lower amount of possible concepts but much higher quality of relation between procedures and attributes
	s.concept_code = CODE and u.sab = 'SNOMEDCT_US'
;
--separate insert for devices: devices should be mapped down the hierarchy too.
insert into test_group
select distinct -- parents <3
	i.procedure_id, --icd10 proc
	a.descendant_concept_id as snomed_id, --snomed proc match candidate
	ra.descendant_concept_id as match_on, --matching attribute id
	ra.min_levels_of_separation as ac, --how far down is the mapping of the device, lower number = more precise; 
	a.max_levels_of_separation as depth, --how far down is the mapping of the procedure, less specific is better (avoid adding detalisation)
	x.concept_class_id
from icd10mappings i
join devv5.concept_ancestor a on  --Include all procedure descendants
	i.concept_class_id = 'Procedure' and
	a.ancestor_concept_id = i.attribute_id
	--and substr (i.procedure_code,1,3) = '0R9' --uncomment to test on single code group, useful to check mappings
join icd10mappings x on --place horizontally procedures and devices
	x.procedure_id = i.procedure_id and
	x.concept_class_id = 'Device'
join devv5.concept_ancestor ra on --include descendants of devices
	ra.ancestor_concept_id = x.attribute_id and
	ra.max_levels_of_separation <=3 --limit set because of acceptability and RAM limits reasons
join concept_relationship r on -- find matching pairs of procedures and attributes in SNOMED
	r.concept_id_1 = a.ancestor_concept_id and
	r.concept_id_2 = ra.descendant_concept_id and
	r.invalid_reason is null
join relations e on --ensure validness of relationship; excludes other vocabularies and speeds up search
	e.relationship_id = r.relationship_id
join concept s on 
    s.concept_id=r.concept_id_1 and s.vocabulary_id = 'SNOMED'
join concept s1 on 
    s1.concept_id=r.concept_id_2 and s1.vocabulary_id = 'SNOMED'
join umls.mrconso u on --filter non-international SNOMED, lower amount of possible concepts but much higher quality of relation between procedures and attributes
	s.concept_code = CODE and u.sab = 'SNOMEDCT_US'


;
commit
;
exec DBMS_STATS.GATHER_TABLE_STATS (ownname => USER, tabname => 'test_group', cascade => true);
;
CREATE INDEX idx_test_group
   ON test_group (procedure_id ASC, snomed_id ASC)
   TABLESPACE USERS;
;
CREATE INDEX idx_test_group2
   ON test_group (procedure_id ASC, match_on ASC)
   TABLESPACE USERS;
;
CREATE INDEX idx_test_group3
   ON test_group (procedure_id ASC)
   TABLESPACE USERS;

;

-- sometimes procedures in snomed have multiple related attributes. We only need to keep unique matches for SNOMED attributes
-- A procedure may have for example both 'Upper limb structure' AND 'Vascular structure of forearm', with ICD10PCS procedure having only the second analogue. 
-- to find superfluos attributes we rely on concept_ancestor table; match attribute is considered superfluous if the same match is also made with that attribute's descendant; 
-- therefore, in any chain of ancestorship only the most specific attribute will be kept  

create table incorrect_attributes as
select distinct -- natching pair we check
	ca.procedure_id,
	ca.snomed_id,
	ca.match_on
from test_group ca
join concept_ancestor c on
	c.ancestor_concept_id = ca.match_on and -- all children of the attribute
	c.min_levels_of_separation > 0 -- not the same attribute
join test_group x on
	ca.procedure_id = x.procedure_id and -- exact matching pair is formed with children
	ca.snomed_id = x.snomed_id and
	c.descendant_concept_id = x.match_on
;
merge into test_group t
using incorrect_attributes a
on 
	(
		t.procedure_id = a.procedure_id and
		t.snomed_id = a.snomed_id and
		t.match_on = a.match_on
	)
when matched then update set t.concept_class_id = 'Parent' --sadly, that's the only way I could find to make MERGE-DELETE work in Oracle
delete where t.concept_class_id = 'Parent' --apologies to whoever reads this code now 
;
drop table incorrect_attributes

;
delete from test_group  --remove some of the most often occurent orphans (high level concepts with weird attributes)
where snomed_id in
(
	4077168,40491014,4232102,45767586,4139742,4182371,4234328,4105155,4086440,4233168,
	4116184,4261385,46270722,4067247,36714124,4298432,4323971,4135728,4128316,43531469,
	46270723,4343810,4074250,4258413,4080086,4233180,4260126,4258534,4206989,4080093,
	4259560,4232542,4206239,4232409,4231953,4230433,40479678,4155083,40483634,4108688,
	4078577,4155083,4084318,40483634,4078577,40483214,4279285,40485402,45763719,4232256,
	4140986,4319263,4107021,4333666,4163524,4066536,4248411,4102241,4335324,4080949,
	4070955,40480982,36713064,36713064,37399098,40486477,4144452,40482319,44784222,4162102,
	4068870,4305559,46271895,4302937,37016708,40481418,4244077,40482355,46272006,36713244,
	4328023,40482355,36715390,4302061,4301920,4170422,4325021,4260272,4167869,4167537,
	4340655,4343013,4021436,4302060,4203368,4176103,4100787,4217513,4144095,4069562,
	4298335,4258009,4107737,4234471,4080162,4070852,4049405,37017002,4070582,4032131,
	4029419,4314894,4279760,4030407,4050111,4347410,4073413,4122803,4042902,40479303,
	42872444,4044873,4207455,4070714,4069562,4223107,4291632,4124020,42873014,37017002,
	4174214,4119420,4124007,4232190,42873013,4314894,4083057,4197728,4069230,4083403,
	4302799,4305686,4168821,4225080,4064645,4046930,4172158,4065582,4067936,4168043,
	4151058,4187872,4335302,4334422,4050577,4125474,4125475,4273706,4067726,4058621,
	4072271,4168317,4127899,4067333,40489304,4235673,4032641,4238340,4123424,4239562,
	40488475,4070365,4095787,4218942,40492756,4128711,4144764,4163233,40484980,40491024,
	4068022,4040823,40492764,4238220,4238220,4301002,4301002,4150356,4189520,4207467,
	4029460,4144181,4120060,4141005,4066530,4067100,4066523,4124031,4107824,4070832,
	4174849,4150356,4022173,4239512,4242444,4328103,4329777,4333973,4078731,4162561,
	4301907,4306681,4128903,4241316,42709815,42709826,4209348,4216343,4285426,4121240,
	4260830,4132734,4075278,4288012,4057073,4075278,4125175,4296476,4324958,4236837,
	4165066,4069344,4177323,4196038,4231534,4085114,4226948,40479702,4173486,4234947,
	45769219,4171218,4303977,4308579,4330384,4197635,4149357,4236171,4314806,4177195,
	4291191,4320141,4109005,4029620,4045857,4137133,4170091,4302968,36713298,4172324,
	40492460,4170795,4179806,4233576,4045616,4104744,40480228,4079586,4093340,4167187,
	4046999,4117707,4340644,4231361,4141616,4219483,4170570,4169618,4304119,4329784,
	4098880,46270919,4070970,4164519,4284672,4299365,4134274,4063526,4063662,4052738,
	4144914,4031492,4176803,40488001,4021276,4107260,4260660,4074153,4235200,4012181,
	37016997,4230269,4078246,4236908,4179377,4346613,4029292,4310405,40489986,4058598,
	4340649,4021115,4129850,4303514,4094443,4171546,4277606,4121816,4097143,4104016,
	40482821,4075282,4075282,4150886,4143503,44783694,4066899,4170719,40487935,40490441,
	4030836,4032132,4237615,4287403,4027897,4234327,40491371,4221550,4322141,4197246,
	4175325,4231644,40479754,40489949,4261527,4072048,4078084,4093276,4078411,4076304,
	4195461,4231668,4234951,4076421,40481495,40484624,44618008,4030239,4142527,4150553,
	4072145,4084008,4078583
	
) --list is formed empirically by revising resulting data
;
--drop table imaging;
drop table guidance;
;
--these groups of concepts contain only false concepts OR create more wrong mappings than right OR include procedure with morph abnorm focus(nonexistent in icd10pcs)
create table guidance as --Imaging Guidance, Endoscopic guidance, resection+anastomosis, endoscopic procedures, intest stoma repair, thrombolyses, incision and drainage, 
						--drainage of abscess, replacement of prosthesis, biopsy of brain, excision of tumors, angioect+anastomos, embolectomy, thrombectomy, laser discectomy,
						--excisional biopsy, shunt from brain to other site, palliative procedures, coagulation, intracranial nerve destruction, procedure on arterioven. malf
						--crushing, open biopsy, myocutaneous flap transplant, incisional biopsy, reimplantation, replacement of cast,marsupializationn, cautherization,
						--cryotherapy, biopsy, plastic excision of skin, atherectomy
	select descendant_concept_id --insert descendants of incorrect procedures 
	from concept_ancestor a
	join concept c on 
		descendant_concept_id = concept_id and
		vocabulary_id = 'SNOMED' 
	where ancestor_concept_id in
	(
		4121042,4312592,4230238,4179713,4263633,4051022,
		4231553,4211374,4040923,4293033,4045859,4152745,
		4187501,4215779,4185284,4076423,4228202,4287375,
		4176643,4001216,4045615,4150452,4246082,4002731,
		4030537,4321986,4151924,4118888,4040382,4099751,
		4146273,4311405,4077087,4284964
	);
;
insert into guidance --stereotactic device, radiofrequency probe, arteriovenous fistula, blood clot, 
					--radiotherapy planning, doppler device, tumor, polyp, surgical biopsy, inversion, cyst, 
					--foreign body, exostosis, thrombus, calculus, varix, laceration, redundant tissue,
					--developmental abnormality
	select r.concept_id_1 --insert procedures having incorrect attributes (or their child attributes)
	from concept_relationship r
	join concept c on 
		c.concept_id = r.concept_id_1 and 
		c.concept_class_id = 'Procedure' and
		c.vocabulary_id = 'SNOMED' and
		c.invalid_reason is null
	join relations e on
		e.relationship_id = r.relationship_id
	join concept_ancestor a on	
		r.concept_id_2 = a.descendant_concept_id and
		a.ancestor_concept_id in (
			4133890,4134654,4029967,4192740,4044940,
			4183725,4030314,4187628,4235039,4260668,
			4279699,4078373,4215424,4245118,4209441,
			4029830,4148390,4043698,4064671
		)

;
delete from test_group 
where 
	snomed_id in (select descendant_concept_id from guidance)
;
--SNOMED 'replacements' are both 'removals' and 'placements', so we ensure that no separate 'insertion' or 'removal' in ICD10PCS will get coded as 'replacement'
create table removal_id as --for removals
	(
		select i.procedure_id 
		from icd10mappings i
		where
			(--removal surgical; code pattern 0.P....
				substr (i.procedure_code,1,1) = '0' and
				substr (i.procedure_code,3,1) = 'P'
			) 
			or
			(--removal of device; code pattern 2.5....
				substr (i.procedure_code,1,1) = '2' and
				substr (i.procedure_code,3,1) = '5'
			)
			or/*
			(--destructive procedures; code pattern 0.5.... -- brachytherapy belongs to other chapter
				substr (i.procedure_code,1,1) = '0' and
				substr (i.procedure_code,3,1) = '5'
			)
			or */
			substr (i.procedure_code,1,1) = 'B' --whole imaging chapter. eliminates whole angiographic stent insertion stuff while we are at it
	)
;
delete from test_group 
where
	procedure_id in (select * from removal_id) and
	snomed_id in (select descendant_concept_id from concept_ancestor where ancestor_concept_id = 4027403) --introduction
;
drop table removal_id
;
create table insertion_id as -- for insertions
	(
		select i.procedure_id 
		from icd10mappings i
		where
			(--insertion surgical; pattern 0.H.... or 1.H....
				substr (i.procedure_code,1,1) in ('0','1') and
				substr (i.procedure_code,3,1) = 'H'
			)
	)
;
delete from test_group 
where
	procedure_id in (select * from insertion_id) and
	snomed_id in (select descendant_concept_id from concept_ancestor where ancestor_concept_id in (4042150,4300185,4000731)) --removal, transplantation, grafting
;
drop table insertion_id
;
create table radio_id as -- delete brachytherapy from surgical destruction -- has it's own chapter
	(
		select i.procedure_id 
		from icd10mappings i
		where
			(--destruction surgical; pattern 0.5....
				substr (i.procedure_code,1,1) = '0' and
				substr (i.procedure_code,3,1) = '5'
			)
	)
;
delete from test_group
where
	procedure_id in (select * from radio_id) and
	snomed_id in (select descendant_concept_id from concept_ancestor where ancestor_concept_id = 4029715) --radiotherapy
;
drop table radio_id
;
create table anatomical as --does procedure relate to anatomical site in any of its mappings? if yes, all match candidates with no relation to body structure should be dropped
	(
		select 
			procedure_id,
			snomed_id,
			ac,
			min (ac) over (partition by procedure_id) as min_ac
		from test_group
		where concept_class_id = 'Body Structure'
	)
;
exec DBMS_STATS.GATHER_TABLE_STATS (ownname => USER, tabname => 'anatomical', cascade => true);
;
CREATE INDEX idx_anatomical
   ON anatomical (procedure_id ASC, snomed_id ASC)
   TABLESPACE USERS;
;
CREATE INDEX idx_anatomical2
   ON anatomical (procedure_id ASC, ac ASC)
   TABLESPACE USERS;
;
CREATE INDEX idx_anatomical3
   ON anatomical (procedure_id ASC)
   TABLESPACE USERS;

;
delete from anatomical a
where --where topography is less precise than possible
	a.ac > a.min_ac
;
delete from test_group --delete mapping variants with no or less precise topography
where
	procedure_id in (select procedure_id from anatomical) and
	(procedure_id,snomed_id) not in (select procedure_id,snomed_id from anatomical)
;
drop table anatomical;


drop table maxfix; --we keep only matches with the highest number of matched attributes 
create table maxfix
	(
		procedure_id number,
		snomed_id number
	)
;
insert into maxfix
	with x as
		(
			select distinct  -- count number of matched attributes for every match pair
				procedure_id,
				snomed_id,
				count (match_on) as c 
			from test_group
			group by procedure_id,snomed_id
		),
	maxim as
		(
			select distinct procedure_id, max(c) as m  --define highest number for every ICD10PCS procedure
			from x
			group by procedure_id
		)
	select x.procedure_id,x.snomed_id from x 
	join maxim ma on 
		ma.m > x.c and
		ma.procedure_id = x.procedure_id
;
drop table group_mid
;
create table group_mid as --remove those with less matches
	(
		select t.* from test_group t
		left join maxfix m on
			m.procedure_id = t.procedure_id and
			m.snomed_id = t.snomed_id
		where m.procedure_id is null
	)
;
exec DBMS_STATS.GATHER_TABLE_STATS (ownname => USER, tabname => 'group_mid', cascade => true);
;
create INDEX idx_group_mid
   ON group_mid (procedure_id ASC, snomed_id ASC)
   TABLESPACE USERS;
;
create INDEX idx_group_mid2
   ON group_mid (procedure_id ASC, ac ASC)
   TABLESPACE USERS;
;
create INDEX idx_group_mid3
   ON group_mid (procedure_id ASC)
   TABLESPACE USERS;
;
commit
;
drop table minfix; --attempt to find closest matches by demanding lowest possible sum of distances between all attributes in matching pairs
create table minfix
	(
		procedure_id number,
		snomed_id number
	)
;
insert into minfix	
	with x as
		(
			select  -- find sum of distances for every match pair
				procedure_id,
				snomed_id,
				sum (ac)
					over (partition by procedure_id, snomed_id) as c
			from group_mid
		),
	minim as --find lowest possible sum for every ICD10PCS procedure
		(
			select distinct procedure_id, min(c) as m
			from x
			group by procedure_id
		)
	select x.procedure_id,x.snomed_id from x -- create a list of all pairs that have higher sum
	join minim mi on 
		mi.m < x.c and
		mi.procedure_id = x.procedure_id;
		

drop table group_finalised;
create table group_finalised as -- recreate table without pairs with higher sums
	(
		select distinct 
			t.procedure_id,
			t.snomed_id,
			t.depth, 
			sum (ac)
				over (partition by t.procedure_id, t.snomed_id) as missed
		from group_mid t
		left join minfix m on
			m.procedure_id = t.procedure_id and
			m.snomed_id = t.snomed_id
		where m.procedure_id is null
	)
;
create table mindeep as -- among all remaining matches, keep the most generic procedure (highest ancestor in procedure tree); table stores minimaly possible level of depth among all matches
		(
			select distinct procedure_id, min(depth) as m
			from group_finalised
			group by procedure_id
		)
;
drop table mappings
;
create table mappings -- final resulting table, subject for manual checks
	(
		procedure_id number,
		snomed_id number,
		rel_id varchar (127)
	)
;
insert into mappings
select distinct
	g.procedure_id,
	g.snomed_id,
	case -- determine mapping precision, propose relatoinship type accordingly -- should not be used at current state
		when missed = 0 then 'Maps to'
		else 'Is a'
	end
from group_finalised g
join mindeep m on 
	g.procedure_id = m.procedure_id and
	g.depth = m.m
;
drop table mindeep;
